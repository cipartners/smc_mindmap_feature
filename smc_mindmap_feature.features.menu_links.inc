<?php
/**
 * @file
 * smc_mindmap_feature.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function smc_mindmap_feature_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_mindmaps:mindmaps
  $menu_links['main-menu_mindmaps:mindmaps'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'mindmaps',
    'router_path' => 'mindmaps',
    'link_title' => 'Mindmaps',
    'options' => array(
      'identifier' => 'main-menu_mindmaps:mindmaps',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 30,
    'customized' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Mindmaps');


  return $menu_links;
}
